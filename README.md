### A STATIC SITE GENERATOR FOR LFBUILDERS

This tool was designed to move an old wordpress site into the cloud

---

# Commands

To start the extraction process be sure to adopt required variables into the config.json file. These variables are used to detect and extract all required data.

Move into the working directory

```
cd path/to/directory
```

Execute

```
python3 main.py
```

This will create a working static version of the website and place it into either a "production" directory or "development" depending on the mode chosen.

---

#### Config example

Here is a basic development version of the config.json file

```
{
  "mode": "development",
  "entry": "./src/index.js",
  "url": "http://localhost/daniel",
  "source_url": "https://lfbuilders.ca",
  "sitemap_xml": "https://lfbuilders.ca/page-sitemap.xml",
  "broken_stylesheets": [
    "solid.min.css",
    "brand.min.css",
    "elementor-icons.min.css",
    "regular.min.css",
    "all.min.css"
  ]
}
```

Only two modes are currently available

    - "production"
    or
    - "development"


