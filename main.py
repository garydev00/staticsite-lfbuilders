print('''
####################################################################################################################################
#                                       A STATIC SITE GENERATOR FOR LFBUILDERS.CA                                                  #
####################################################################################################################################
''')
import requests
import re as regex
import os
import json
import configparser
import urllib.request
from bs4 import BeautifulSoup

with open('config.json') as f:
    config = json.load(f)
    f.close()

url = config['url']
mode = config['mode'] + "/"
source_url = config['source_url']
sitemap_lfbuilders = config['sitemap_xml']
broken_stylesheets = config['broken_stylesheets']
####################################################################################################################################
# CREATE CUSTOM HEADERS. THESE VARIABLES MAY NEED TO CHANGE DEPENDING ON WEBSERVER FUNCTIONALITY.                                  #
####################################################################################################################################
config = configparser.ConfigParser()
config.read('headers.ini')
headers = config['browser-default']
headers = dict(headers)

####################################################################################################################################
# CREATE CONTACT DIRECTORY AND COPY ALL CONTENTS OF THE CURRENT CONTACT DIRECTORY TO THE NEW DIRECTORY.                            #
# THE CONTACT DIRECTORY CONTAINS ALL THE FILES THAT ARE NEEDED TO CREATE THE CONTACT PAGE.                                         #
# INCLUDING A JAVASCRIPT INTERFACE AND PHP MAILER.(THESE ARE CUSTOM PRE BUILT JAVASCRIPT AND PHP FILES)                            #
####################################################################################################################################
if not os.path.exists(mode + 'contact'):
    os.makedirs(mode + 'contact')
for i in os.listdir('contact'):
    if i != '.gitkeep':
        os.system('cp -r contact/' + i + ' ' + mode + 'contact/')

####################################################################################################################################
# A SIMPLE REGEX SUBSTITUTION FUNCTION THAT REPLACES ALL THE URLS WITH THE LOCAL URLS.                                             #
# ONLY PASS READ OR WRITE FILES AS ARGUMENTS.                                                                                      #
####################################################################################################################################
def replace_urls_css(lib_dir):
    with open(lib_dir, 'r') as f:
        content = f.read()
        content = regex.sub(r'https://lfbuilders.ca', 'https://web.lfbuilders.ca', content)
        f.close()
    with open(lib_dir, 'w') as f:
        f.write(content)
        f.close()

####################################################################################################################################
# A LIB DOWNLOADER FUNCTION THAT USES A CUSTOM SET OF HEADERS TO DOWNLOAD A FILE FROM A URL TO A LOCAL DIRECTORY                   #
####################################################################################################################################
def lib_downloader(url, lib_dir):
    # a lib downloading wrapper that uses a custom set of headers.
    global headers
    try:
        hostname = url.split('/')[2]
        opener = urllib.request.build_opener()
        opener.addheaders = [('User-Agent', headers['user-agent'])]
        opener.addheaders = [('Accept-Encoding', headers['accept-encoding'])]
        opener.addheaders = [('Accept-Language', headers['accept-language'])]
        opener.addheaders = [('Content-Length', headers['content-length'])]
        opener.addheaders = [('Content-Type', headers['content-type'])]
        opener.addheaders = [('Device-Memory', headers['device-memory'])]
        opener.addheaders = [('Downlink', headers['downlink'])]
        opener.addheaders = [('Dpr', headers['dpr'])]
        opener.addheaders = [('Ect', headers['ect'])]
        opener.addheaders = [('Referer', headers['referer'])]
        opener.addheaders = [('Rtt', headers['rtt'])]
        opener.addheaders = [('Sec-Ch-Prefers-Color-Scheme', headers['sec-ch-prefers-color-scheme'])]
        opener.addheaders = [('Sec-Ch-Ua', headers['sec-ch-ua'])]
        opener.addheaders = [('Host', hostname)]
        opener.addheaders = [('Sec-Ch-Ua-Mobile', headers['sec-ch-ua-mobile'])]
        opener.addheaders = [('Sec-Ch-Ua-Model', headers['sec-ch-ua-model'])]
        opener.addheaders = [('Sec-Ch-Ua-Platform', headers['sec-ch-ua-platform'])]
        opener.addheaders = [('Sec-Ch-Ua-Platform-Version', headers['sec-ch-ua-platform-version'])]
        opener.addheaders = [('Sec-Fetch-Dest', headers['sec-fetch-dest'])]
        opener.addheaders = [('Sec-Fetch-Mode', headers['sec-fetch-mode'])]
        opener.addheaders = [('Sec-Fetch-Site', headers['sec-fetch-site'])]
        opener.addheaders = [('Sec-Fetch-User', headers['sec-fetch-user'])]
        opener.addheaders = [('Upgrade-Insecure-Requests', headers['upgrade-insecure-requests'])]
        opener.addheaders = [('Viewport-Width', headers['viewport-width'])]
        urllib.request.install_opener(opener)
        urllib.request.urlretrieve(url, lib_dir)
        #check if the file is a css file and if it is, then replace the urls with the local ones.
        if ".css" in url:
            replace_urls_css(lib_dir)

    except Exception as e:
        print(e)

#############################################################################################################################
# A CSS URL GETTER FUNCTION THAT GETS ALL THE CSS LINKS FROM A SINGLE CSS FILE AND DOWNLOADS THEM TO THE LOCAL DIRECTORY    #
#############################################################################################################################
def css_url_getter(css, dir):
    global source_url

    usl_spl = css.split(';')

    for i in usl_spl:
        if 'url(' in i:
            #tries to get all files attached to home url inside CSS file
            if "lfbuilders.ca" in i:
                url = i.split('url(')[1].split(')')[0]
                url = url.replace('"', '')
                print("URL: " + url)
                print(dir)
                dir_url = url.split('/')
                dir_url = dir_url[:-1]
                dir_url = '/'.join(dir_url)
                dir_url = dir_url.replace(source_url+'/', '')
                print("DIR URL: " + dir_url)
                file_url = url.split('/')[-1]
                print("DOWNLOADING TO: " + mode + dir_url + '/' + file_url)
                if not os.path.exists(mode + dir_url):
                    os.makedirs(mode + dir_url)
                if not os.path.exists(mode + dir_url + '/' + file_url):
                    lib_downloader(url, mode + dir_url + '/' + file_url)
                    print("DOWNLOADED IMAGE FILE TO: " + mode + dir_url + '/' + file_url)
            #tries to get all FONTS attached to home url inside CSS file
            if "font" in i:
                url = i.split('url(')[1].split(')')[0]
                list_url = url.split('/')
                file_name = list_url[-1]
                file_name = file_name.split('?')[0]
                second_dir = ""
                for j in range(len(list_url) - 1):
                    if ".." in list_url[j]:
                        continue
                    second_dir += list_url[j] + '/'
                spl_bracket = url.split('/')
                sub_dir = 0
                for i in spl_bracket:
                    if ".." in i:                            
                        sub_dir += 1
                directory = dir.split('/')
                directory = list(filter(None, directory))
                directory = directory[:-sub_dir]
                directory = '/'.join(directory)
                base_url = directory.split('/')
                base_url[0] = source_url
                base_url = '/'.join(base_url)
                if not os.path.exists(directory):
                    os.makedirs(directory)
                if not os.path.exists(directory + "/" + second_dir + file_name):
                    lib_downloader(base_url + "/" + second_dir + file_name, directory + "/" + file_name)
                    print("DOWNLOADED CSS FILE TO: " + directory + "/" + second_dir + file_name)
                else:
                    return

####################################################################################################################################
# A SOURCE EXTRACTION FUNCTION THAT GETS ALL THE SOURCE LINKS FROM A SINGLE HREF OR URL AND DOWNLOADS THEM TO THE LOCAL DIRECTORY  #
####################################################################################################################################
def src_ripper(i):
    global mode
    global source_url
    dir = i.split('/')
    dir = dir[:-1]
    dir = '/'.join(dir)
    dir = dir.replace(source_url+'/', '')
    re = requests.get(i)
    file = i.split('/')[-1]
    if "?" in file:
        file = file.split('?')[0]
    master_dir = mode + dir + '/'
    if ".css" in file:
        css_url_getter(re.text, master_dir)
    try:
        os.makedirs(master_dir)
    except FileExistsError:
        pass
    lib_downloader(i, master_dir + file)

####################################################################################################################################
# REMOVE ALL BROKEN STYLESHEETS FROM HTML FILE                                                                                     #
####################################################################################################################################
def broken_stylesheet_fix(soup):
    #remove broken stylesheets
    global broken_stylesheets
    for i in broken_stylesheets:
        try:
            stylesheets = soup.find_all('link', {"rel": "stylesheet"})
            for j in stylesheets:
                if i in j.get('href'):
                    j.decompose()
        except:
            pass

####################################################################################################################################
# A FIX ALGORITHM THAT REPLACES ELEMENTOR FONTAWESOME ICONS WITH PUBLIC ONE                                                        #
####################################################################################################################################
def fontawesome_fix(soup):
    iclass = soup.find_all('i')
    for i in iclass:
        i_class = ' '.join(i['class'])
        if "elementor-menu-toggle__icon--open eicon-menu-bar" in i_class:
            i['class'] = []
        if "elementor-menu-toggle__icon--close eicon-close" in i_class:
            i['class'] = []
            i['class'] = "fa fa-bars"
        if "fas fa-caret-down" in i_class:
            i['class'] = []
            i['class'] = "fa fa-caret-down"
        if "fas fa-envelope" in i_class:
            i['class'] = []
            i['class'] = "fa fa-envelope"
        if "fas fa-phone-alt" in i_class:
            i['class'] = []
            i['class'] = "fa fa-phone"
        if "fas fa-envelope" in i_class:
            i['class'] = []
            i['class'] = "fa fa-envelope"
        if "fas fa-map-marker-alt" in i_class:
            i['class'] = []
            i['class'] = "fa fa-map-marker"
        if "fas fa-clock" in i_class:
            i['class'] = []
            i['class'] = "fa fa-clock"
        if "fas fa-caret-right" in i_class:
            i['class'] = []
            i['class'] = "fa fa-caret-right"
        if "fas fa-at" in i_class:
            i['class'] = []
            i['class'] = "fa fa-at"
        if "fab fa-facebook-f" in i_class:
            i['class'] = []
            i['class'] = "fa fa-facebook-f"
        if "fab fa-instagram" in i_class:
            i['class'] = []
            i['class'] = "fa fa-instagram"

####################################################################################################################################
# A UTILITY FUNCTION THAT INJECTS SITE REQUIRED UTILITIES INTO THE SOURCE CODE                                                     #
####################################################################################################################################
def add_to_head(soup):
    head = soup.find('head')
    link = soup.new_tag('link')
    link['href'] = 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'
    link['rel'] = 'stylesheet'
    head.append(link)
    script = soup.new_tag('script')
    script['src'] = 'https://code.jquery.com/jquery-3.3.1.min.js'
    head.append(script)
    script = soup.new_tag('script')
    script['src'] = 'contact/app.js'
    head.append(script)

####################################################################################################################################
# A CUSTOM SITEMAP GENERATOR FUNCTION THAT REPLACES ALL THE SITE LINKS WITH THE LOCAL LINKS                                        #
####################################################################################################################################
def lfbuilders_sitemap_tool():
    global url
    global source_url
    global sitemap_lfbuilders
    print("[*] COLLECTING SITEMAP...")
    if url[-1] == "/":
        url_sitemap = url[:-1]
    else:
        url_sitemap = url
    lib_downloader(sitemap_lfbuilders, mode + "/page-sitemap.xml")
    with open(mode + "/page-sitemap.xml", 'r') as f:
        sitemap = f.read()
    sitemap = sitemap.replace(source_url, url_sitemap)
    with open(mode + "/page-sitemap.xml", 'w') as f:
        f.write(sitemap)
    print("[*] COLLECTING FEED...")
    lib_downloader(source_url + "/feed/", mode + "feed")
    with open(mode + "feed", 'r') as f:
        feed = f.read()
    feed = feed.replace(source_url, url_sitemap)
    with open(mode + "feed", 'w') as f:
        f.write(feed)
    print("[*] COLLECTING ROBOTS.TXT...")
    lib_downloader(source_url + "/robots.txt", mode + "robots.txt")
    with open(mode + "robots.txt", 'r') as f:
        robots = f.read()
    robots = robots.replace("https://lfbuilders.ca/sitemap_index.xml", url_sitemap + "/page-sitemap.xml")
    with open(mode + "robots.txt", 'w') as f:
        f.write(robots)

####################################################################################################################################
# MAIN FUNCTION THAT RUNS THE PROGRAM                                                                                              #
####################################################################################################################################
if __name__ == "__main__":
    lfbuilders_sitemap_tool()
    re = requests.get(sitemap_lfbuilders)
    soup = BeautifulSoup(re.text, 'html.parser')
    #get all location tags from sitemap
    locs = soup.find_all('loc')
    pages = {}
    for i in locs:
        if i.text.split('/')[3] == '':
            filename = 'index'
        else:
            filename = i.text.split('/')[3]
        
        pages[filename] = i.text

    for key, value in pages.items():
        re = requests.get(value)
        filename = key + '.html'
        soup = BeautifulSoup(re.text, 'html.parser')
        links = soup.find_all('link')
        for i in links:
            if "wp-content" in i.get('href'):
                src_ripper(i.get('href'))
            if "wp-includes" in i.get('href'):
                src_ripper(i.get('href'))
        sources = regex.findall(r'src="(.*?)"', str(soup))
        for i in sources:
            if "wp-content" or "wp-includes" in i:
                src_ripper(i)
        imgs = soup.find_all('img')
        for i in imgs:
            if "wp-content" in i.get('src'):
                src_ripper(i.get('src'))
            if "wp-includes" in i.get('src'):
                src_ripper(i.get('src'))
        scripts = soup.findAll('script',{"src":True})
        for i in scripts:
            if "wp-content" in i['src']:
                src_ripper(i['src'])
            if "wp-includes" in i['src']:
                src_ripper(i['src'])
        divs = soup.find_all('div', {"data-thumbnail":True})
        for i in divs:
            src_ripper(i.get('data-thumbnail'))

        navigations = soup.find_all('a')
        for l in navigations:
            try:
                i = l.get('href')
                dir_check = i.split('/')
                for j in dir_check:
                    if j in pages:
                        print(j)                    
                        if 'production' in mode:
                            print("Production mode.")
                            n = url + '/' + j + '.html'
                            print("[*] ADDING N: " + n)
                        else:
                            print("Development mode.")
                            n = url + mode + j + '.html'
                            print(n)
                        l['href'] = n         
            except:
                pass

        buttons = soup.find_all('button')
        for i in buttons:
            span = i.find_all('span')
            for j in span:
                if "Send" in j.text:
                    i['class'] = 'php-mailer-button'
                    break
                if "Free Inquiry" in j.text:
                    i['class'] = 'php-mailer-button-free-inquiry'
                    break

        #fix font awesome
        fontawesome_fix(soup)
        #fix broken stylesheets
        broken_stylesheet_fix(soup)
        #add utilities to head
        add_to_head(soup)
        #remove useless wpemoji library
        scripts = soup.find_all('script')
        for i in scripts:
            if "window._wpemojiSettings" in i.text:
                #remove the script
                i.decompose()

        ###############################################################
        # TODO: ADD TO BROKEN STYLESHEET FIX FUNCTION                 #                                                           
        #find the link that contains the string "css/brands.min.css"  #
        links = soup.find_all('link')                                 #
        for i in links:                                               #
            if "css/brands.min.css" in i.get('href'):                 #
                #remove the link                                      #
                i.decompose()                                         #
        ###############################################################

    
        #stringify the soup for regex purposes
        content = str(soup.prettify())
        #final substitution to insure all remaining links are configured
        if 'production' in mode:
            content = regex.sub(source_url + '/', url + '/', content)
        else:
            content = regex.sub(source_url + '/', '/' + url + mode, content)
        #remove all form and post methods
        content = regex.sub('<form class="elementor-form" method="post" name="New Form">', '', content)
        content = regex.sub('</form>', '', content)
        #write to file
        with open(mode + filename, 'w') as f:
            f.write(content)
            f.close()
        print("[*] DOWNLOADED " + filename)

    print("[!] DONE, STATIC SITE DOWNLOADED TO " + mode)

        








