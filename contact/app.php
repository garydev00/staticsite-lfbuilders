<?php
//A small php script to handle the contact form.
$is_ajax = 'xmlhttprequest' == strtolower( $_SERVER['HTTP_X_REQUESTED_WITH'] ?? '' );
//get data
if($is_ajax){
    $name = $_POST['name'];
    $email = $_POST['email'];
    $phone = $_POST['phone'];
    $selection = $_POST['selection'];
    $message = $_POST['message'];
    $dir = dirname(__FILE__);
    require $dir . '/components_2_7/src/GarySendInBlue.php';
    $result = new GarySendInBlue($name, $email, $phone, $selection, $message, 1);
    $result = $result->Send();
    echo json_encode(array("status" => "success", "message" => "Message sent successfully", "result" => $result));
    die();
}
if(!$is_ajax){
    echo json_encode(array("status" => "failed", "message" => "oops, something went wrong."));
    die();
}
?>