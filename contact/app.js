$(document).ready(function () {
  home_mail_btn = $(".php-mailer-button");
  if (home_mail_btn.length > 0) {
    home_mail_btn.click(function () {
      ajax_mail_tool($(this));
    });
  }
  page_mail_btn = $(".php-mailer-button-free-inquiry");
  if (page_mail_btn.length > 0) {
    page_mail_btn.click(function () {
      ajax_mail_tool($(this));
    });
  }

  function ajax_mail_tool($ele_btn) {
    //get the form data
    try {
      if ($ele_btn.hasClass("php-mailer-button-free-inquiry")) {
        if (home_mail_btn.length > 0) {
          disolver = home_mail_btn.parent().parent();
          disolver.remove();
        }
      }

      var page_url = window.location.href;
      var ele_btn = $ele_btn;
      mail_name =
        $("#form-field-name").val() ?? "inquiry from page " + page_url;
      mail_email =
        $("#form-field-email").val() ?? "inquiry from page " + page_url;
      mail_email =
        $("#form-field-email").val() ?? "inquiry from page " + page_url;
      mail_phone =
        $("#form-field-field_1").val() ?? "inquiry from page " + page_url;
      mail_selection =
        $("#form-field-field_2").val() ?? "inquiry from page " + page_url;
      mail_message =
        $("#form-field-message").val() ?? "inquiry from page " + page_url;
      empty_flag = false;
      if (mail_name == "" && $("#form-field-name").is(":focusable")) {
        $("#form-field-name").css("border", "1px solid red");
        empty_flag = true;
        //end
        return;
      }
      if (mail_email == "" && $("#form-field-email").is(":focusable")) {
        $("#form-field-email").css("border", "1px solid red");
        empty_flag = true;
        //end
        return;
      }
      if (mail_phone == "" && $("#form-field-field_1").is(":focusable")) {
        $("#form-field-field_1").css("border", "1px solid red");
        empty_flag = true;
        //end
        return;
      }
      if (mail_message == "" && $("#form-field-field_2").is(":focusable")) {
        $("#form-field-message").css("border", "1px solid red");
        empty_flag = true;
        //end
        return;
      }
      if (!empty_flag) {
        $.ajax({
          url: "contact/app.php",
          type: "POST",
          data: {
            name: mail_name,
            email: mail_email,
            phone: mail_phone,
            selection: mail_selection,
            message: mail_message,
          },
          success: function (data) {
            response = JSON.parse(data);
            console.log(response.status);
            if (response.status == "success") {
              parent_ele = ele_btn.parent();
              if ($("#form-field-name").length > 0) {
                $("#form-field-name").parent().remove();
              }
              if ($("#form-field-email").length > 0) {
                $("#form-field-email").parent().remove();
              }
              if ($("#form-field-field_1").length > 0) {
                $("#form-field-field_1").parent().remove();
              }
              if ($("#form-field-field_2").length > 0) {
                $("#form-field-field_2").parent().remove();
              }
              if ($("#form-field-message").length > 0) {
                $("#form-field-message").parent().remove();
              }
              ele_btn.remove();

              parent_ele.append(
                "<div class='success-message' style='color: whitesmoke;'>" +
                  "<p>Thank you for your message, " +
                  mail_name +
                  "</p>" +
                  "<p>We will get back to you as soon as possible.</p>" +
                  "<p>Have a great day!</p>" +
                  "</div>"
              );
              //end
            }
          },
          error: function (data) {
            console.log("an error occured. " + data);
          },
        });
      }
    } catch (error) {
      console.log(error);
    }
  }
});
