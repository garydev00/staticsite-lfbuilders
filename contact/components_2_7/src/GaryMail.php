<?php
class GaryMailer {
    function __construct( $host, $port, $secure, $user, $pass) {
        $this->dir = dirname(__FILE__);
        $this->sendTo = $this->get_emails();
        $this->host = $host;
        $this->port = $port;
        $this->secure = $secure;
        $this->user = $user;
        $this->pass = $pass;
    }
    private function get_emails() {
        $json = file_get_contents($this->dir . '/maillist.json');
        $data = json_decode($json, true);
        $list = $data['emails'];
        return $list;
    }

    public function get_object_vars() {
        return get_object_vars($this);
    }

    public function send_mail($type) {
        require_once($this->dir . '/Exception.php');
        require_once($this->dir . '/PHPMailer.php');
        require_once($this->dir . '/SMTP.php');
        $mail = new PHPMailer\PHPMailer\PHPMailer();
        $mail->IsSMTP(); 
    
        $mail->CharSet="UTF-8";
        $mail->Host = $this->host;
        $mail->SMTPDebug = 0; 
        $mail->Port = $this->port; 
        $mail->SMTPSecure = $this->secure;  
        $mail->SMTPAuth = true; 
        $mail->IsHTML(true);
    
        //Authentication
        $mail->Username = $this->user;
        $mail->Password = $this->pass;
        
        // //Set Params
        foreach($this->sendTo as $key => $value) {
            $mail->SetFrom($this->user);
            $mail->addReplyTo($this->user);
            $mail->AddAddress($value);

            $mail->Subject = "TEST";
            $mail->msgHTML("FOOBAR! ADD SMTP CAPABILITIES");
            if(!$mail->Send()) {
                $this->log_mailer_errors($date . " | " . $mail->ErrorInfo);
                return "An error occured during execution. Please confirm all mailing options are correct.";
            } else {
                $this->log_mailer_errors($date . " | SENT TO: " . $value . " | " . $type);
            } 
        }
        return "MAIL: process completed succesfully.";
    }

    private function log_mailer_errors( $_error ){
      $fn = $this->dir . '/error.log';
      $fp = fopen($fn, 'a');
      fputs($fp, "Mailer | an error occured: " . $_error . " | \n");
      fclose($fp);
    }

}

?>