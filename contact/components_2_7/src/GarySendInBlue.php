<?php
class GarySendInBlue {
    // A small wrapper for the SendInBlue mail API
    private $maillist = array("TODO: create mail list");
    function __construct(string $name, string $email, string $phone, string $selection, string $message, int $type) {
        $this->DestinationEmail = $email;
        $this->DestinationName = $name;
        $this->DestinationPhone = $phone;
        $this->DestinationSelection = $selection;
        $this->DestinationMessage = $message;
        $this->Type = $type;
    }

    private function SetHeaders() {
        $headers = array();
        $headers[] = 'Accept: application/json';
        $headers[] = 'Api-Key: ';
        $headers[] = 'Content-Type: application/json';
        return $headers;
    }

    private function SetBody() {
        $body = array();
        $body['sender'] = array(
            'name' => 'donotreply@lfbuilders.ca',
            'email' => "donotreply@lfbuilders.ca",
        );
        $body['to'] = array(
            array(
                'email' => "",
                'name' => 'lead-gen',
            ),
        );
        //get time
        $date = date('Y-m-d H:i:s');
        $body['subject'] = 'new lead ' . $date;
        $body['htmlContent'] = '
        <html>
        <head></head>
        <body>
        <h1>Garrett-Lead-Generation-Tool.(GLGT v0.1)</h1>
        <p>Name: ' . $this->DestinationName . '</p>
        <p>Email: ' . $this->DestinationEmail . '</p>
        <p>Phone: ' . $this->DestinationPhone . '</p>
        <p>Selection: ' . $this->DestinationSelection . '</p>
        <p>Message: ' . $this->DestinationMessage . '</p>
        <p>Time: ' . $date . '</p>
        <p>Type: ' . $this->Type . '</p>
        </body></html>
        ';
        return $body;
    }

    public function Send() {
        $code = array();
        foreach ($this->maillist as $user) {
            $body = $this->SetBody();
            $body['to'][0]['email'] = $user;
            $headers = $this->SetHeaders();
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://api.sendinblue.com/v3/smtp/email');
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($body));
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $server_output = curl_exec($ch);
            curl_close($ch);
            $result = json_decode($server_output, true);
            $code[] = $result;
        }
        return $code;
    }
}

?>